#include <stdint.h>
#include <stdbool.h>
#include <inc/tm4c123gh6pm.h>

void adc_init(void);
int adc_read(void);

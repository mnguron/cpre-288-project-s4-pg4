function [angleList, distList] = readScan(client)
    angleList = [];
    distList = [];
    %Cybot last used = 10
    while true
        
            str = readline(client);
            
            try
                [angle, dist] = str.strip.split;
            catch
                break;
            end
            
            if (angle(1) == "Degrees" )
                continue;
            elseif (angle(1) == "")
                break;
            end
            
            angleList(end+1) = angle(1);
            distList(end+1) = angle(2);
        
    end
end

